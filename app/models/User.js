class User {
    constructor({id, pseudo}) {
        this.id = id;
        this.pseudo = pseudo;
    }
}

module.exports = User;