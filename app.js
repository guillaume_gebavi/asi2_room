'use strict';

global.CONFIG = require('./config.json');

const app = require('express')();
const express = require('express');
const http = require('http').Server(app);
const io = require('socket.io')(http);
const roomManager = require('./app/services/RoomManager');

app.use(express.static(CONFIG.publicDir));
/*
io.on('connection', (socket) =>{

    //io.listen(CONFIG.portSocket);

    let rm = new roomManager(io, socket);

    // si l'utilisateur choisit de se connecter à une room de libre (Matchmaking)
    rm.connectToFreeRoom();

});
*/

let numroom = 1;
io.on('connection', function(socket) {

    if(io.of('/').adapter.rooms["room-"+numroom] && io.of('/').adapter.rooms["room-"+numroom].length > 1)
        numroom++;

    socket.join("room-"+numroom);

    socket.on('roomSignal', (socket1) =>{
        console.log("[FROM-CLIENT] " + socket1 + ":" + socket.id);
        let lastRoomLength = io.of('/').adapter.rooms["room-"+numroom].length;

        if(lastRoomLength==2){
            socket.emit('roomFull', numroom);
            console.log("Room " + numroom + " pleine");
        }
    });

    //Envoyer l'évènement à tout le monde dans le room
    io.sockets.in("room-"+numroom).emit('connectToRoom', numroom);
});


http.listen(CONFIG.port, function() {
    console.log('listening on localhost:' + CONFIG.port);
});